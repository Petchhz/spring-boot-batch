package com.example.batchprocessing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class BatchProcessingApplication {
/*
    @Autowired
    private PersonRepository repository;
    
    @Autowired
    private ReportService service;

    @GetMapping("/getPersons")
    public List<Person> getPersons() {

        return repository.findAll();
    }

    @GetMapping("/report/{format}")
    public String generateReport(@PathVariable String format) throws FileNotFoundException, JRException {
        return service.exportReport(format);
    }

    public static void main(String[] args) {
        SpringApplication.run(BatchProcessingApplication.class, args);
    }
    }*/
	public static void main(String[] args) throws Exception {

		SpringApplication.run(BatchProcessingApplication.class, args);
	
 }
}
