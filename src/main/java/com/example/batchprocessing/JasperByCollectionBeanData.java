
package com.example.batchprocessing;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;
import java.sql.SQLException;

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import static net.sf.jasperreports.engine.JRParameter.REPORT_CONNECTION;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;

import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import net.sf.jasperreports.engine.design.JasperDesign;

import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;


public class JasperByCollectionBeanData {
    
    public static void main(String[] args) throws JRException, FileNotFoundException, ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/person?useSSL=false&serverTimezone=UTC&useLegacyDatetimeCode=false","root","p075517072");
        String reportPath = "C:\\Users\\user\\JaspersoftWorkspace\\MyReports\\person.jrxml";
        JasperReport jasperreport = JasperCompileManager.compileReport(reportPath);
        JasperPrint jasperprint = JasperFillManager.fillReport(jasperreport,null,con);
        JasperViewer.viewReport(jasperprint);
        JasperExportManager.exportReportToPdfFile(jasperprint, "C:\\Users\\user\\Desktop\\Reported\\Person_Report.pdf");
        con.close();
        System.out.println("File Generated");
    }    
}
